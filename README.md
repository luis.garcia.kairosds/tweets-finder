# tweets-finder

Search tweets

## Tools

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)
* [NodeJs](https://nodejs.org/es/)
* [Docker](https://www.docker.com/)


## Run

### Java Tweets finder api

Utiliza el api de twitter Streaming para recibir tweets en tiempo real y guardar los que cumplen los requisitos.



*  Run with maven

```sh
$ cd tweets-finder-api
$ ./mvnw spring-boot:run
```


### Node Gateway and Auth

*  Run with Node

```sh
$ cd auth-gateway-service
$ node index.js
```

## Docker

*  Build Docker Image

Java Api

```sh
$ cd tweets-finder-api
$ ./mvnw package
```

* Run docker-compose

```sh
$ docker-compose up
```


## Use

● Login para obtener token

> POST http://localhost:3000/auth/login


```json
{
    "token": "fa118eb0-4e86-40f4-862f-780e3187003a"
}
```

Añadir token en las llamadas con el header
> x-api-key: fa118eb0-4e86-40f4-862f-780e3187003a


● Consultar los tweets.

> GET http://localhost:3000/twitter/api/tweets/

● Marcar un tweet como validado.

> PUT http://localhost:3000/twitter/api/tweets/{id}/validate

● Consultar los tweets validados por usuario.

> GET http://localhost:3000/twitter/api/tweets/validated

● Consultar una clasificación de los N hashtags más usados (default 10).

> GET http://localhost:3000/twitter/api/tweets/hashtags?n=10
