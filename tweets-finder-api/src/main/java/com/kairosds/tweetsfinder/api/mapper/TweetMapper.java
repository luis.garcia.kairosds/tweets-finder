package com.kairosds.tweetsfinder.api.mapper;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.kairosds.tweetsfinder.api.model.Tweet;

import twitter4j.HashtagEntity;
import twitter4j.Status;

@Component
public class TweetMapper {

	public Tweet fromStatus(Status status) {
		return Tweet.builder().id(status.getId()).text(status.getText())
				.username(status.getUser().getName())
				.location(status.getGeoLocation())
				.userfollowers(status.getUser().getFollowersCount())
				.hashtags(Arrays.asList(status.getHashtagEntities()).stream().map(HashtagEntity::getText).collect(Collectors.toList()))
				.build();
	}
}
