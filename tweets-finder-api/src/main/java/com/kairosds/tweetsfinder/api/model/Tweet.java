package com.kairosds.tweetsfinder.api.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;
import twitter4j.GeoLocation;

@Data
@Builder
@Document
public class Tweet {

	@Id
	private long id;
	
	private String text;
	
	private GeoLocation location;
	
	private String username;
	
	private int userfollowers;
	
	private boolean validated;
	
	private List<String> hashtags;
	

}
