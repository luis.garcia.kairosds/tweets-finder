package com.kairosds.tweetsfinder.api.repository;

import java.util.List;

import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.kairosds.tweetsfinder.api.model.HashtagCount;
import com.kairosds.tweetsfinder.api.model.Tweet;

public interface TweetRepository extends MongoRepository<Tweet, Long> {

	List<Tweet> findByValidatedIsTrue();
		
	List<Tweet> removeById(String id);
	
	@Aggregation(pipeline = {"{$unwind:'$hashtags'}","{ $group: { _id : $hashtags, n : { $sum : 1 } } }","{$sort:{n:-1}}","{$limit: ?0 }"})
	AggregationResults<HashtagCount> groupByLastnameAnd(int n);
	
}
