package com.kairosds.tweetsfinder.api.runner;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.kairosds.tweetsfinder.api.mapper.TweetMapper;
import com.kairosds.tweetsfinder.api.service.TweetService;

import lombok.extern.slf4j.Slf4j;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

@Component
@Slf4j
public class TwitterStreamingRunnerService implements ApplicationRunner {

	@Value(value = "${allowed.languages:es,fr,it}")
	private String [] languages;
	
	@Value(value = "${min.followers.count:1500}")
	private int minFollowersCount;
	
	private TweetService tweetServiceImpl;
	
	private TweetMapper tweetMapper;
		
	@Autowired
	public TwitterStreamingRunnerService(TweetService tweetServiceImpl, TweetMapper mapper) {
		this.tweetServiceImpl = tweetServiceImpl;
		this.tweetMapper = mapper;
	}
	
	@Override
	public void run(ApplicationArguments args) {
		System.out.println("Runable");
		StatusListener listener = new StatusListener() {

			@Override
			public void onException(Exception ex) {
				ex.printStackTrace();
			}

			@Override
			public void onStatus(Status status) {
				log.debug(status.getUser().getName() + " : " + status.getText());
				
				if(status.getUser().getFollowersCount()>minFollowersCount) {
					tweetServiceImpl.saveTweet(tweetMapper.fromStatus(status));
				}
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
			}

			@Override
			public void onStallWarning(StallWarning warning) {
			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
				// TODO Auto-generated method stub

			}
		};
		log.info("Starting twitter listener");
		log.info("Filter languajes" + Arrays.toString(languages));
		log.info("Filter users by min followers: " + minFollowersCount);
		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		twitterStream.addListener(listener);

		FilterQuery query = new FilterQuery();
		query.language(languages);
		double[][] locations = { { -180, -90 }, { 180, 90 } };
		query.locations(locations);
		twitterStream.filter(query);

	}


}
