package com.kairosds.tweetsfinder.api.model;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class HashtagCount {

	@Id
	private String hashtag;
	private int n;
}
