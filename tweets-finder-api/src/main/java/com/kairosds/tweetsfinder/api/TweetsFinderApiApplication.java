package com.kairosds.tweetsfinder.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TweetsFinderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TweetsFinderApiApplication.class, args);
	}

}
