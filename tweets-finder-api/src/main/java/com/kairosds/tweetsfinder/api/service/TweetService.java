package com.kairosds.tweetsfinder.api.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kairosds.tweetsfinder.api.model.HashtagCount;
import com.kairosds.tweetsfinder.api.model.Tweet;

public interface TweetService {

	Page<Tweet> getTweets(Pageable pageable);

	Tweet getTweet(Long id);

	List<Tweet> getValidatedTweets();

	List<HashtagCount> getHashtags(int n);

	Tweet validate(Long id);

	Tweet saveTweet(Tweet tweet);

}