package com.kairosds.tweetsfinder.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;

import com.kairosds.tweetsfinder.api.model.HashtagCount;
import com.kairosds.tweetsfinder.api.model.Tweet;
import com.kairosds.tweetsfinder.api.repository.TweetRepository;
import com.kairosds.tweetsfinder.api.service.TweetService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TweetServiceImpl implements TweetService {

	private TweetRepository repository;
	
	@Autowired
	public TweetServiceImpl(TweetRepository repository) {
		this.repository = repository;
	}

	@Override
	public Page<Tweet> getTweets(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Tweet getTweet(Long id) {
		return repository.findById(id).orElseThrow();
	}

	@Override
	public List<Tweet> getValidatedTweets() {
		return repository.findByValidatedIsTrue();
	}

	@Override
	public List<HashtagCount> getHashtags(int n) {
 		log.info("Seerch hastags " + n);
		AggregationResults<HashtagCount> result = repository.groupByLastnameAnd(n); 
		return result.getMappedResults();
	}

	@Override
	public Tweet validate(Long id) {
		Tweet tweet = this.getTweet(id);
		tweet.setValidated(true);
		return repository.save(tweet);
	}
	
	@Override
	public Tweet saveTweet(Tweet tweet) {
		return repository.save(tweet);
	}
	
}
