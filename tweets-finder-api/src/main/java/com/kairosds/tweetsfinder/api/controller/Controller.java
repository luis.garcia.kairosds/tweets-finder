package com.kairosds.tweetsfinder.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kairosds.tweetsfinder.api.model.HashtagCount;
import com.kairosds.tweetsfinder.api.model.Tweet;
import com.kairosds.tweetsfinder.api.service.TweetService;

@RestController
@RequestMapping("/tweets")
public class Controller {

	private TweetService tweetService;

	@Autowired
	public Controller(TweetService tweetService) {
		this.tweetService = tweetService;
	}

	@GetMapping()
	public Page<Tweet> get(Pageable pageable) {
		return tweetService.getTweets(pageable);
	}

	@GetMapping("/{id}")
	public Tweet get(@PathVariable Long id) {
		return tweetService.getTweet(id);
	}

	@GetMapping("/validated")
	public List<Tweet> getValidatedTweets() {

		return tweetService.getValidatedTweets();
	}

	@GetMapping("/hashtags")
	public List<HashtagCount> getHastaghs(@RequestParam(defaultValue = "10") int n) {
		return tweetService.getHashtags(n);
	}

	@PutMapping("/{id}/validate")
	public Tweet validate(@PathVariable Long id) {
		return tweetService.validate(id);
	}

}
