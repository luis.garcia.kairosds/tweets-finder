var express = require('express');
var router = express.Router();

var uuid = require('uuid');
var tokens = require('./tokens');

router.post('/login', function(req, res) {
    var id = uuid.v4();
    tokens.add(id);
    res.send({token:id});
});

module.exports = router;