var TokenUtils = module.exports = {
    tokens: [],
    add: function(token) {
        TokenUtils.tokens.push(token);
    },
    isValid:function(token){
        return TokenUtils.tokens.indexOf(token)>-1;
    }
}