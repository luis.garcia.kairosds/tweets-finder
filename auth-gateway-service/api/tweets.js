var express = require('express');
var router = express.Router();

var proxy = require('express-http-proxy');

var tokens = require('./tokens');

const TWEET_API_URL = process.env.TWEET_API_URL || 'localhost:8080';

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {

    var token = req.header('x-api-key');
    console.log('token: ',token);
    console.log(tokens.isValid(token));

    if(!token){
        res.status(401).send('Header x-api-key not found');
    }

    if(!tokens.isValid(token)){
        res.status(403).send('Invalid Token');
    }

    next();
});


router.all('/api/*', function(req,res,next){
    console.log('url:', req.url);
    next();
}, proxy( TWEET_API_URL, {
    proxyReqPathResolver: function (req) {
        console.log('proxy-url',TWEET_API_URL);
        var parts = req.url.split('?');
        var queryString = parts[1];
        var updatedPath = parts[0].replace(/\/api/, '');
        var redirectUrl = updatedPath + (queryString ? '?' + queryString : '');
        console.log('sending request to', redirectUrl);
        return redirectUrl;
    },proxyErrorHandler: function(err, res, next) {
        console.error(err);
        next(err);
    } 

})

);



module.exports = router;